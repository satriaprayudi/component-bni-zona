# component-bni-zona

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

# Bit

## Add component
```
bit add src/components/ProductList/ProductList.vue --id bni-zona
bit add src/components/ProductList/products.js --id bni-zona
```

## Build component
```
bit build
```

## Tag component
```
bit tag --all 0.0.1
```

## Export component
```
bit export <username>.<scope>
```


## Import component
```
bit import @bit/<username>.<scope>/bni-zona
or
npm i @bit/<username>.<scope>.bni-zona --save
```
