import axios from 'axios'

export async function getData () {
    const res = await axios
    .get('https://content.digi46.id/promos')
    .then(response => {
        return { status: true, data: response.data }
    })
    .catch(error => {
        return { status: false, data: error}
    })
    .finally(() => { return {status: false, data: null} })
    return res
}